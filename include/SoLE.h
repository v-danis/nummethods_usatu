#pragma once
#include "Matrix.h"
#include <fstream>
#include <iostream>
#include <algorithm>
#include <random>
#include <iomanip>
#include "gnuplot-iostream.h"
using namespace std;
#define func(x) ((x)*(x)*(x)*sqrt(1-(x)*(x))) // �������� �������
#define Delta 0.000001 // ������ �����������

class SoLE
{
private:
    int n;
    int p;
    int q;
    vector<pair<double, double>> XY;
    vector<vector<double>> matrix;
    vector<double> root;
    bool ReadFromFile();
    void PrintMatrix();
    void createMatrix(int N);
    double RationalValue(double x);
    void SaveDataFile(string gnuplotiostreamFile, vector<vector<double>> data, vector<vector<double>> input);
public:
    SoLE(int n);
    SoLE();
    //
    void diag(int N);
    void Gauss(vector<vector<double>> A, vector<double> B);
    void LU();
    void RationalInterpolation(int p, int q, double a, double b, string gnuplotiostreamFile);
};
#pragma once
#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#define _USE_MATH_DEFINES
#include <math.h>
#include <cmath>
#include <fstream>
#include "gnuplot-iostream.h"
using namespace std;

#define Taylor(x) (pow(x,3) - 1./2. * pow(x,5) - 1./8. * pow(x,7) - 1./16. * pow(x,9) - 5./128. * pow(x,11) - 7./256. * pow(x,13))
#define y(x) ((x)*(x)*(x)*sqrt(1-(x)*(x))) // �������� �������
#define yTrig(t,a,b) (a + (b-a)*t/(2.0*M_PI)) // �������� ������ ���������� [0;2*pi]
#define Delta 0.000001 // ������ �����������

// ����� ������������
class Interpolation
{
private:
	int n; // ���������� ����� ������������
	int N; // ���������� ����������� �����
	bool bull; // ���� ��������� ��� �������
	bool flag; // ���� �����������/������������� �����
	double a; // ������ �������
	double b; // ����� �������
	double delta; // ��� �������
	double errorb; // ���������� �����������
	vector<double> x_arr; // ������ ���������� X ��������� ����� ������������
	vector<double> y_arr; // ������ �������� Y ��������� ����� ������������
	vector<double> t_arr; // ������ �������� ���������� t 
	vector<double> x_for_pol; // ������ ���������� X ��������� ����������� �����
	vector<double> errors_vect; // ������ ������ � ������ �����
	vector<vector<double>> data; // ������ ����� ������������ � ������� (x, y)
	vector<vector<double>> output; // ������ ����������� ����� � ������� (x, y)

private:
	// ������� ��� ���������� ����������� ���������� ������ 
	void ComputeError();

	// �������, ��������� ������� ������ ��� ���������������� ���� ������������

	// �������� �������� ������ ����
	void CreateDataSet();
	// �������� �������� ��� ���������� �������� (�����������/������������� �����)
	void CreateDataSetLagranj(); 
	// �������� �������� ��� ���������� �������
	void CreateDataSetNewton(vector<vector<double>>& finite_differences);
	// �������� �������� ��� ������������������ ������������
	void CreateDataSetTrigonometric(vector<double>& A_arr, vector<double>& B_arr);
	// �������� �������� ��� ������������ ���������
	void CreateDataSetSpline(vector<double>& A, vector<double>& B, vector<double>& C, vector<double>& D);

	// �������, ����������� ��������� ������

	// ���������� �������� ����������������� ���������� �������� � �.x
	void LagranjCalculation(double x, vector<vector<double>>& output);
	// ���������� �������� ����������������� ���������� ������� � �.x
	void NewtonCalculation(double x, const vector<vector<double>>& finite_differences, vector<vector<double>>& output);
	// ���������� �������� ������������������� ����������������� ���������� � �.t
	void TrigonometricCalculation(double t, const vector<double>& A_arr, const vector<double>& B_arr, vector<vector<double>>& output);
	// ���������� �������� ����������� ������� � �.x
	void SplineCalculation(double x, const vector<double>& A, const vector<double>& B, const vector<double>& C,
		const vector<double>& D, vector<vector<double>>& output);
public:
	// ������ �����������
	Interpolation(); 
	// �����������, ����������� ������ � ����� �������
	Interpolation(double a, double b); 

	// ������� ��� ���������� � ������������ ���������� ������
	void SaveDataFile(string gnuplotiostreamFile); 
	// ������� ��� ������������ ������� ����������� ������ �� ������� ���������� 
	void ErrorGraphics(string gnuplotiostreamFile, vector<vector<double>> errorsVec);
	// �������, ������������ ��������� �������� ������������ �����������
	double GetErrorb(); 

	// �������, ���������� ��� ���������� ������� ��������������� ������������

	// ���������������� ��������� ��������
	void Lagranj(int n, int N, bool flag);
	// ���������������� ��������� �������
	void Newton(int n, int N, bool flag = false);
	// ������������������ ������������
	void Trigonometric(int n, int N, bool flag = true);
	// ������������ ���������
	void Spline(int n, int N, bool flag = false);
	// ��������� ����������� �����������
	void BestApproximation(vector<vector<double>> &vec, int n = 10, int N = 100000);
	// ��������������� �����
	void Telescope();
};
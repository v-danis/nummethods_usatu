#pragma once
#include "Matrix.h"
#include "gnuplot-iostream.h"
#include <iostream>
#include <algorithm>
#include <functional>
#include <iterator>
#include <iomanip>  
#define _USE_MATH_DEFINES
#include <math.h>
#include <cmath>
#include <fstream>
#include <string>
#include <map>
#include <random>
using namespace std;
#define y(x) ((x)*(x)*(x)*sqrt(1-(x)*(x))) // �������� �������
//#define y(x)(sin(x))
#define yGauss(t,a,b) ((a + b) / 2. + (b - a) / 2.0 * t) // �������� ������ ���������� [-1;1]
#define Delta 0.000001 // ������ �����������

// ���������� ������� �������
typedef map <string, function<void(int)>> func;

class Integration
{
private:
	int n; // ���������� �����
	func f; // ������� �������
	double a; // ������ �������
	double b; // ����� �������
	double p; // ������� �������� ���������
	double delta; // ��� ���������
	double errorb; // ���������� ����������� (���� ������ (���. error ���))
	double result; // ��������� ���������� ��������������
	double mapleResult; // ��������� ������� �������������� � Maple
	vector<double> x_arr; // ������ ���������� X ��������� �����
public:
	// ������ �����������
	Integration(); 
	// �����������, ����������� ������ � ����� �������
	Integration(double a, double b); 
	// ������� ������ ��������� ������� � ����
	void WriteToFile();
	// ������� ��������� ���������� �������������� �� �����
	bool ReadFromFile();
	// ������� ��� ���������� ����������� ���������� ������ 
	void ComputeError(); 

	void OptimalN(string name, string gnuplotiostreamFile, bool limit = false, bool graphics = false, int start = 5, int steps = 10 , int end = 100);

	void ErrorGraphics(string gnuplotiostreamFile, vector<vector<double>> errorsVec, int start = 5, int steps = 10);
	// �������� �������� ������ ����
	void CreateDataSet();

	double Legendre(int n, double x);

	double diffLegendre(int n, double x);

	vector<double> LegendreRoot(int n);

	
	// �������

	double getP();
	double getResult();
	double getErrorb(); 
	double getMapleResult();

	// �������, ����������� ������ ���������� ��������������

	void Rectangle(int n); // ������� ��������������� (������� ��������)
	void Trapezoid(int n); // ������� ��������
	void Simpson(int n); // ������� �������� (������� �������)
	void Aitken(int n, double k, string name); // ������� �������
	void Runge(int n1, int n2, string name, double k = 0.5); // ������� �����
	void Gauss(int n); // ������������ ������� ������
	void Romberg(int n, double k, string name, double q = 2.); // ������� �������� 
	void MonteCarlo(int n); // ����� �����-�����
};
#pragma once
#include <iostream>
#include <vector>
#include <cstdlib>
#include <time.h>
#include <cmath>
#include <fstream>
#include <iomanip>
#include "Matrix.h"
using namespace std;
#define Delta 0.000001

class ISMSofLAE
{
public:
	ISMSofLAE();
private:
	void printMatrix(vector<vector<double>>& M);
	void printVector(vector<double>& V);
	vector<double> JacobiMethod(int N, int lw, vector<vector<double>> &A, vector<double> &b);
	void Norm(int N, vector<double> &solTrue, vector<double> &solOur);
	vector<double> SORLenta(int N, int l, vector<vector<double>> &A, vector<double> &b, double &relaxation);
};


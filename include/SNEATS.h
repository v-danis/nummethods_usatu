#pragma once
#include <iostream>
#include <algorithm>
#include <iomanip>
#include <vector>
#include <map>
#include <functional>
#include <cmath>
#define y(x) (1 - 2*(x)*cos(x)) // �������� �������
#define fi(x) ((2*(x)*(x)*sin(x)-1)/(2.*(x)*sin(x) - 2*cos(x))) 
//#define f_1(x_0, y_0) (-cbrt((y_0 * cos(x_0 * y_0)) / 4.)) 
//#define f_2(x_0, y_0) (-(x_0) * cos(x_0 * y_0) / 2.)
#define f_1(x_0, y_0) (0.5 / pow((x_0 + 2*y_0), 2)) 
#define f_2(x_0, y_0) (1. / pow((x_0 + 2*y_0), 2))
#define df(x) (-2.*cos(x)+2.*(x)*sin(x))
#define Delta 0.000001 // ������ �����������
using namespace std;
typedef map <string, function<void(double, double)>> methods;
class SNEATS
{
private:
	methods f;
	vector<pair<int, double>> roots;
	pair<int, pair<double, double>> system_roots;
	void Bisection(double a, double b);
	void Secant(double a, double b);
	void Secant_n(double a, double b);
	void FixedPointIteration(double a, double b = 0.);
	void Newton(double a, double b);
public:
	SNEATS();
	void Roots(double a, double b, int n, string name);
	void PrintRoots();
	void PrintSystemRoots();
	void FixedPointIterationSystem(double x_0, double y_0);
};



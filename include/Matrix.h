#pragma once
#include <vector>
#include <math.h>

class Matrix
{
public:
    static double Determinant(std::vector<std::vector<double>> matrix);
    static void Inversion(std::vector<std::vector<double>>& A);
    static std::vector<std::vector<double>> Multiplication(std::vector<std::vector<double>> A, std::vector<std::vector<double>> B);
    static std::vector<double> VectorMultiplication(std::vector<std::vector<double>> A, std::vector<double> B);
    static std::vector<std::vector<double>> Tranpose(std::vector<std::vector<double>>& M);
    static std::vector<std::vector<double>> MatrixToLenta(int N, int l, std::vector<std::vector<double>> Matrix, std::vector<std::vector<double>> MatrixLenta);
    static std::vector<double> LentaVectorMultiplication(int N, int lw, std::vector<std::vector<double>> MatrixLenta, std::vector<double> Vector);
    static double VectorVectorMultiplication(int N, std::vector<double> Vector1, std::vector<double> Vector2);
};
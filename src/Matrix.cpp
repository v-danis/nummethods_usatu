#include "Matrix.h"

double Matrix::Determinant(std::vector<std::vector<double>> matrix)
{
    int i, j, k, r;
    double c, M, max, s, det = 1;
    for (k = 0; k < matrix.size(); k++)
    {
        max = fabs(matrix[k][k]);
        r = k;
        for (i = k + 1; i < matrix.size(); i++)
            if (fabs(matrix[i][k]) > max)
            {
                max = fabs(matrix[i][k]); r = i;
            }
        if (r != k) det = -det;
        for (j = 0; j < matrix.size(); j++)
        {
            c = matrix[k][j]; matrix[k][j] = matrix[r][j]; matrix[r][j] = c;
        }
        for (i = k + 1; i < matrix.size(); i++)
            for (M = matrix[i][k] / matrix[k][k], j = k; j < matrix.size(); j++)
                matrix[i][j] -= M * matrix[k][j];
    }
    for (i = 0; i < matrix.size(); i++)
        det *= matrix[i][i];
    return det;
}

void Matrix::Inversion(std::vector<std::vector<double>>& A)
{
    int n = A.size();
    double temp;
    std::vector<std::vector<double>> E;
    E.resize(n);
    for (auto& el : E)
        el.resize(n);

    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
        {
            E[i][j] = 0.0;

            if (i == j)
                E[i][j] = 1.0;
        }

    for (int k = 0; k < n; k++)
    {
        temp = A[k][k];

        for (int j = 0; j < n; j++)
        {
            A[k][j] /= temp;
            E[k][j] /= temp;
        }

        for (int i = k + 1; i < n; i++)
        {
            temp = A[i][k];

            for (int j = 0; j < n; j++)
            {
                A[i][j] -= A[k][j] * temp;
                E[i][j] -= E[k][j] * temp;
            }
        }
    }

    for (int k = n - 1; k > 0; k--)
    {
        for (int i = k - 1; i >= 0; i--)
        {
            temp = A[i][k];

            for (int j = 0; j < n; j++)
            {
                A[i][j] -= A[k][j] * temp;
                E[i][j] -= E[k][j] * temp;
            }
        }
    }

    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            A[i][j] = E[i][j];
}

std::vector<std::vector<double>> Matrix::Multiplication(std::vector<std::vector<double>> A, std::vector<std::vector<double>> B)
{
    int n = A.size();
    std::vector<std::vector<double>> result(n, std::vector<double> (n,0));
    for (int row = 0; row < n; row++) {
        for (int col = 0; col < n; col++) {
            for (int j = 0; j < n; j++) {
                result[row][col] += A[row][j] * B[j][col];
            }
        }
    }
    return result;
}

std::vector<double> Matrix::VectorMultiplication(std::vector<std::vector<double>> A, std::vector<double> B)
{
    std::vector<double> result(A.size());
    for (int i = 0; i < A.size(); i++)
    {
        for (int j = 0; j < A.size(); j++)
        {
            result[i] = result[i] + A[i][j] * B[j];
        }
    }
    return result;
}

std::vector<std::vector<double>> Matrix::Tranpose(std::vector<std::vector<double>>& M)
{
    std::vector<std::vector<double>> transposedMatrix(M[0].size(), std::vector<double>(M.size(), 0));
    for (int i = 0; i < M.size(); i++)
    {
        for (int j = 0; j < M[0].size(); j++)
        {
            transposedMatrix[j][i] = M[i][j];
        }
    }
    return transposedMatrix;
}

std::vector<std::vector<double>> Matrix::MatrixToLenta(int N, int l, std::vector<std::vector<double>> Matrix, std::vector<std::vector<double>> MatrixLenta)
{
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            if (Matrix[i][j] != 0)
                MatrixLenta[i][j - i + l] = Matrix[i][j];
        }
    }
    return MatrixLenta;
}

std::vector<double> Matrix::LentaVectorMultiplication(int N, int lw, std::vector<std::vector<double>> MatrixLenta, std::vector<double> Vector)
{
    std::vector<double> VectorSolution(N, 0);
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            int ind = j - i + lw;
            if ((ind >= 0) && (ind < 2 * lw + 1))
            {
                VectorSolution[i] += MatrixLenta[i][ind] * Vector[j];
            }
        }
    }
    return VectorSolution;
}

double Matrix::VectorVectorMultiplication(int N, std::vector<double> Vector1, std::vector<double> Vector2)
{
    double result = 0;
    for (int i = 0; i < N; i++)
    {
        result += Vector1[i] * Vector2[i];
    }
    return result;
}

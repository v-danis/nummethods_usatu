#include "ISMSofLAE.h"

ISMSofLAE::ISMSofLAE()
{
    int N = 700;
    int l = 16;
 
    srand(time(NULL));
    vector<vector<double>> A1(N, vector<double>(N, 0)); //��������� �������
    vector<vector<double>> A2(N, vector<double>(N, 0));
    vector<vector<double>> A3(N, vector<double>(N, 0));
    vector<vector<double>> AT1(N, vector<double>(N, 0)); //����������������� �������
    vector<vector<double>> AT2(N, vector<double>(N, 0));
    vector<vector<double>> AT3(N, vector<double>(N, 0));
    vector<vector<double>> M1(N, vector<double>(N, 0)); //������� �� * �
    vector<vector<double>> M2(N, vector<double>(N, 0));
    vector<vector<double>> M3(N, vector<double>(N, 0));

    double q1 = 1.1;
    double q2 = 2;
    double q3 = 10;

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            if ((l + 1) - abs(i - j) > 0) {
                //��������� ��������������� ��������� ��������� ������� �
                A1[i][j] = (double)(rand()) / RAND_MAX * (1 - (-1)) + (-1);
            }
            A2[i][j] = A1[i][j];
            A3[i][j] = A1[i][j];
        }
    }

    //������� ������������ ������������
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            A1[i][i] += fabs(A1[i][j]);
            A2[i][i] += fabs(A2[i][j]);
            A3[i][i] += fabs(A3[i][j]);
        }
        A1[i][i] *= q1;
        A2[i][i] *= q2;
        A3[i][i] *= q3;
    }

    //����� ������, ��� �����, ��� ��� ���������� ������ �����������
    //printMatrix(A1);
    //printMatrix(A2);
    //printMatrix(A3);


    vector<double> sol(N, 0); //c������� � ��������� ������� ������� ������� ����
    for (int i = 0; i < N; i++)
    {
        sol[i] = (double)(rand()) / RAND_MAX * (1 - (-1)) + (-1);
    }

    //cout << "Solution" << endl;
    //printVector(sol);

    //���������� �������� ������ ����� �������
    vector<double> rhs1 = Matrix::VectorMultiplication(A1, sol);
    vector<double> rhs2 = Matrix::VectorMultiplication(A2, sol);
    vector<double> rhs3 = Matrix::VectorMultiplication(A3, sol);

    //����������������� �������
    AT1 = Matrix::Tranpose(A1);
    AT2 = Matrix::Tranpose(A2);
    AT3 = Matrix::Tranpose(A3);

    //AT*A : 
    M1 = Matrix::Multiplication(AT1, A1);
    M2 = Matrix::Multiplication(AT2, A2);
    M3 = Matrix::Multiplication(AT3, A3);

    //b*:
    vector<double> b1 = Matrix::VectorMultiplication(AT1, rhs1);
    vector<double> b2 = Matrix::VectorMultiplication(AT2, rhs2);
    vector<double> b3 = Matrix::VectorMultiplication(AT3, rhs3);

    vector<double> x1(N, 0);
    vector<double> x2(N, 0);
    vector<double> x3(N, 0);
    cout << endl;

    // ������� ��������� ������� c ������� ����� 4l+1
    vector<vector<double>> Lenta1(N, vector<double>(4 * l + 1, 0));
    vector<vector<double>> Lenta2(N, vector<double>(4 * l + 1, 0));
    vector<vector<double>> Lenta3(N, vector<double>(4 * l + 1, 0));

    //��������� ��������� �������, �� ���� � ��� 1 ������� �������
    Lenta1 = Matrix::MatrixToLenta(N, 2 * l, M1, Lenta1);
    Lenta2 = Matrix::MatrixToLenta(N, 2 * l, M2, Lenta2);
    Lenta3 = Matrix::MatrixToLenta(N, 2 * l, M3, Lenta3);
  /*  printMatrix(M1);
    cout << endl; cout << endl; cout << endl;
    printMatrix(Lenta1);*/
    cout << "__________________" << endl;
    cout << endl;


    cout << "2. Jacobi method iterations and solutions: " << endl;
    cout << " -------------------------- " << endl;
    cout << "For q1 = " << q1 << " ";
    x1 = JacobiMethod(N, 2 * l, Lenta1, b1);
    Norm(N, x1, sol);

    cout << "For q2 = " << q2 << " ";
    x2 = JacobiMethod(N, 2 * l, Lenta2, b2);
    Norm(N, x2, sol);

    cout << "For q3 = " << q3 << " ";
    x3 = JacobiMethod(N, 2 * l, Lenta3, b3);
    Norm(N, x3, sol);
    cout << endl;

    cout << "__________________" << endl;
    cout << endl;


    cout << "3. SOR method iterations and solutions: " << endl;
    double w1 = 0.2; //���� �������� w �� 0 �� 2
    double w2 = 1;
    double w3 = 1.9;

    cout << " relax = " << w1 << endl;
    cout << " For q1 = " << q1 << " "; x1 = SORLenta(N, 2 * l, Lenta1, b1, w1);
    Norm(N, x1, sol);
    cout << " For q2 = " << q2 << " "; x2 = SORLenta(N, 2 * l, Lenta2, b2, w1);
    Norm(N, x2, sol);
    cout << " For q3 = " << q3 << " "; x3 = SORLenta(N, 2 * l, Lenta3, b3, w1);
    Norm(N, x3, sol);

    cout << endl;
    cout << " relax = " << w2 << endl;
    cout << " For q1 = " << q1 << " "; x1 = SORLenta(N, 2 * l, Lenta1, b1, w2);
    Norm(N, x1, sol);
    cout << " For q2 = " << q2 << " "; x2 = SORLenta(N, 2 * l, Lenta2, b2, w2);
    Norm(N, x2, sol);
    cout << " For q3 = " << q3 << " "; x3 = SORLenta(N, 2 * l, Lenta3, b3, w2);
    Norm(N, x3, sol);
    cout << endl;

    cout << " relax = " << w3 << endl;
    cout << " For q1 = " << q1 << " "; x1 = SORLenta(N, 2 * l, Lenta1, b1, w3);
    Norm(N, x1, sol);
    cout << " For q2 = " << q2 << " "; x2 = SORLenta(N, 2 * l, Lenta2, b2, w3);
    Norm(N, x2, sol);
    cout << " For q3 = " << q3 << " "; x3 = SORLenta(N, 2 * l, Lenta3, b3, w3);
    Norm(N, x3, sol);
    /*printVector(x1);
    printVector(x2);
    printVector(x3);*/
    cout << endl;

}

void ISMSofLAE::printMatrix(vector<vector<double>>& M)
{
	cout << "-----------------------------------------------" << endl;
	for (auto& row : M)
	{
		for (auto& el : row)
		{
			cout << fixed << setprecision(4) << setw(10) << right << el << "\t";
		}
		cout << endl;
	}
	cout << "-----------------------------------------------" << endl;
}

void ISMSofLAE::printVector(vector<double>& V)
{
	cout << "[ ";
	for (auto& x : V)
	{
		cout << fixed << setprecision(4) << setw(8) << right << x << " ";
	}
	cout << "]" << endl;
}

vector<double> ISMSofLAE::JacobiMethod(int N, int lw, vector<vector<double>>& A, vector<double>& b)
{
	vector<double> xprev(N, 0);
	vector<double> x(N, 0);
	double diff;
	int iter = 0;
	do
	{
		iter++;
		for (int i = 0; i < N; i++)
		{
			x[i] = b[i];
			for (int j = 0; j < N; j++)
			{
				int ind = j - i + lw;
				if ((j != i) && (ind >= 0) && (ind < 2 * lw + 1))
					x[i] -= A[i][ind] * xprev[j];
			}
			x[i] /= A[i][lw];
		}
		diff = fabs(x[0] - xprev[0]);
		for (int i = 0; i < N; i++)
		{
			if (fabs(x[i] - xprev[i]) > diff)
				diff = fabs(x[i] - xprev[i]);
			xprev[i] = x[i];
		}
	} while (diff > Delta);
	cout << "Iterations: " << iter << endl;
	return x;
}

void ISMSofLAE::Norm(int N, vector<double>& solTrue, vector<double>& solOur)
{
	double max = fabs(solOur[0] - solTrue[0]);
	for (int i = 0; i < N; i++)
	{
		if (fabs(solOur[i] - solTrue[i]) > max)
			max = fabs(solOur[i] - solTrue[i]);
	}
	cout << "Norm = " << max << endl;
}

vector<double> ISMSofLAE::SORLenta(int N, int l, vector<vector<double>>& A, vector<double>& b, double& relaxation)
{
	vector<double> xprev(N, 0); // �������oe �����������
	vector<double> x(N, 0); // ������� �����������
	double diff; // ������� ����� ������� � ���������� ������������, �������� ������ �� ������� ������ eps
	int iter = 0;

	do
	{
		iter++;
		for (int i = 0; i < N; i++)
		{
			x[i] = b[i];
			for (int j = 0; j < i; j++)
			{
				int ind = j - i + l;
				if ((j != i) && (ind >= 0) && (ind < 2 * l + 1))
					x[i] -= A[i][ind] * x[j];
			}
			for (int j = i + 1; j < N; j++)
			{
				int ind = j - i + l;
				if ((j != i) && (ind >= 0) && (ind < 2 * l + 1))
					x[i] -= A[i][ind] * xprev[j];
			}
			x[i] /= A[i][l];
			x[i] *= relaxation;
			x[i] += (1 - relaxation) * xprev[i];
		}
		diff = fabs(x[0] - xprev[0]);
		for (int i = 0; i < N; i++)
		{
			if (fabs(x[i] - xprev[i]) > diff)
				diff = fabs(x[i] - xprev[i]);
			xprev[i] = x[i];
		}
	} while (diff > Delta);
	cout << " Iterations: " << iter << endl;
	return x;
}

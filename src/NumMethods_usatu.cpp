﻿#include "NumMethods_usatu.h"
using namespace std;
string gnuplotiostreamFile = "\"D:\\program files\\gnuplot\\bin\\gnuplot.exe\"";

// Определяем тип: функция класса Interpolation method_t, которая принимает аргументы (int, int, bool)
typedef void (Interpolation::* interpol_method)(int, int, bool);
// Определяем словарь функций интерполяции
typedef map<string, pair<interpol_method, bool>> func_map_t;
int OptimalN(string func, Interpolation* polynom, bool graphics = false, bool limit = false) {
    // Словарь функций вида: "Название численного метода" - Функция
    func_map_t mp;
    mp["LagranjTrue"] = { &Interpolation::Lagranj, true };
    mp["LagranjFalse"] = { &Interpolation::Lagranj, false };
    mp["Newton"] = { &Interpolation::Newton, true };
    mp["Trigonometric"] = { &Interpolation::Trigonometric, true };
    mp["Spline"] = { &Interpolation::Spline, true };
    // Поиск ключа в словаре
    func_map_t::iterator x = mp.find(func);
    vector<vector<double>> errorsVec; // Массив ошибок приближения: |f(x)-Ln(x)|
    if (x == mp.end()) // Если такого ключа нет
    {
        cout << "Сan't find such a function, check the request!" << endl;
        system("PAUSE");
        return -1;
    }
    if (x != mp.end() && limit == false) // Если ключ есть, но предельный уровень приближения не рассматривается
    {
        for (int n = 1; n <= 15; n++)
        {
            (polynom->*(x->second.first))(n, 100000, x->second.second);
            errorsVec.push_back({ (double)n, polynom->GetErrorb() });
        }
    }
    else if (x != mp.end()) // Если необходимо рассмотреть предельный уровень приближения
    {
        // Минимальная степень m многочлена 
        int min_m = 2;
        do
        {
            min_m++;
            (polynom->*(x->second.first))(min_m, 100000, x->second.second);
        } while (polynom->GetErrorb() > Delta);
        cout << "Error = " << polynom->GetErrorb() << endl;
        cout << "Minimal m : " << min_m << endl;
        return min_m;
    }
    double min = errorsVec[0][1]; // Минимальная ошибка
    int idx; // Оптимальное значение n
    for (int i = 0; i < errorsVec.size(); i++)
    {
        if (errorsVec[i][1] < min)
        {
            min = errorsVec[i][1];
            idx = errorsVec[i][0];
        }
    }
    cout << "Minimal error: " << min << "\nOptimal n: " << idx << endl;
    if (graphics) { polynom->ErrorGraphics(gnuplotiostreamFile, errorsVec); };
    return idx;
}
int Menu() {
    {
        cout << "\n\n";
        int menu, lab;
        while (1)
        {
            cout << "To complete the entire program, enter the number '0', to continue, enter any number: " << endl;
            cout << "1. Lab #1 - Approximation of functions." << endl;
            cout << "2. Lab #2 - Numerical integration." << endl;
            cout << "3. Lab #3 - Direct solution methods Systems of Linear Algebraic Equations." << endl;
            cout << "4. Lab #4 - Solving Nonlinear Equations and their systems." << endl;
            cout << "5. Lab #5 - Iterative methods for solving systems of linear algebraic equations." << endl;
            cout << "0.I'm tired" << endl;
            cin >> lab;
            system("cls");
            switch (lab)
            {
            case 1:
            {
                int lagranj_n = -1, trigonometric_n = -1, spline_n = -1;
                Interpolation polynom(-0.5, 0.5);
                while (1)
                {
                    cout << "To complete the entire program, enter the number '0', to continue, enter any number: " << endl;
                    cout << "1. #1. Lagrange interpolation." << endl;
                    cout << "2. #2. Lagrange interpolation with Chebyshev polynomials. " << endl;
                    cout << "3. #3. Newton interpolation." << endl;
                    cout << "4. #4. Trigonometric interpolation." << endl;
                    cout << "5. #5. The best uniform approximation." << endl;
                    cout << "6. #6. Interpolation by splines." << endl;
                    cout << "0. Go to Lab" << endl;
                    cin >> menu;
                    system("cls");
                    if (menu == 0) break;
                    switch (menu)
                    {
                    case 1:
                        cout << "To complete the entire program, enter the number '0', to continue, enter any number: " << endl;
                        cout << "1. #1.1 Construction of the Lagrange interpolation polynomial for arbitrary degree 'n'. " << endl;
                        cout << "2. #1.2 & #1.3 determine the optimal degree 'n' at which the error is minimal. " << endl;
                        cout << "3. #1.4 Plot the approximation error" << endl;
                        cin >> menu;
                        if (menu == 0) return 0;
                        if (menu == 1)
                        {
                            cout << "Please enter degree 'n': ";
                            int n;
                            cin >> n;
                            polynom.Lagranj(n, 1e3, true);
                            polynom.SaveDataFile(gnuplotiostreamFile);
                            system("cls");
                        }
                        if (menu == 2)
                        {
                            if (lagranj_n < 0) { lagranj_n = OptimalN("LagranjTrue", &polynom, true); }
                            else
                            {
                                cout << "Optimal n : " << lagranj_n << endl;
                                system("PAUSE");
                            }
                            system("cls");
                        }
                        if (menu == 3)
                        {
                            cout << "This task is still in development mode. See you later!" << endl;
                            system("PAUSE");
                            system("cls");
                        }
                        break;
                    case 2:
                        cout << "To complete the entire program, enter the number '0', to continue, enter any number: " << endl;
                        cout << "1. #2.1 & #2.2 Construction of the Lagrange interpolation with Chebyshev polynomials for arbitrary degree 'n'." << endl;
                        cout << "2. #2.3 Compare the error estimate with the known theoretical one." << endl;
                        cout << "3. #2.4 Perform a comparison of two Lagrange polynomials on uniform and non - uniform grids." << endl;
                        cin >> menu;
                        if (menu == 0) return 0;
                        if (menu == 1)
                        {
                            if (lagranj_n < 0) { lagranj_n = OptimalN("LagranjTrue", &polynom); }
                            else cout << "Optimal n : " << lagranj_n << endl;
                            polynom.Lagranj(lagranj_n, 100000, false);
                            polynom.SaveDataFile(gnuplotiostreamFile);
                            system("cls");
                        }
                        if (menu == 2)
                        {
                            cout << "This task is still in development mode. See you later!" << endl;
                            system("PAUSE");
                            system("cls");
                        }
                        if (menu == 3)
                        {
                            if (lagranj_n < 0) { lagranj_n = OptimalN("LagranjTrue", &polynom); }
                            else cout << "Optimal n : " << lagranj_n << endl;
                            polynom.Lagranj(lagranj_n, 1e3, true);
                            auto ErrorTrue = polynom.GetErrorb();
                            polynom.Lagranj(lagranj_n, 1e3, false);
                            auto ErrorFalse = polynom.GetErrorb();
                            cout << "Maximal error for even grid: " << ErrorTrue << endl;
                            cout << "Maximal error for uneven grid: " << ErrorFalse << endl;
                            if (ErrorTrue > ErrorFalse)
                            {
                                cout << "As you see, it's more accurate on uneven grid." << endl;
                            }
                            else cout << "As you see, it's more accurate on even grid." << endl;
                            system("PAUSE");
                            system("cls");
                        }
                        break;
                    case 3:
                        cout << "To complete the entire program, enter the number '0', to continue, enter any number: " << endl;
                        cout << "1. #3.1 Construction of the Newton interpolation polynomial for arbitrary degree 'n'." << endl;
                        cout << "2. #3.2 Perform a comparison of the constructed Newton polynomial with the analogous Lagrange polynomial." << endl;
                        cin >> menu;
                        if (menu == 0) return 0;
                        if (menu == 1)
                        {
                            if (lagranj_n < 0) { lagranj_n = OptimalN("LagranjTrue", &polynom); }
                            else cout << "Optimal n : " << lagranj_n << endl;
                            polynom.Newton(lagranj_n, 1e3);
                            polynom.SaveDataFile(gnuplotiostreamFile);
                            system("cls");
                        }
                        if (menu == 2)
                        {
                            if (lagranj_n < 0) { lagranj_n = OptimalN("LagranjTrue", &polynom); }
                            else cout << "Optimal n : " << lagranj_n << endl;
                            polynom.Lagranj(lagranj_n, 1e3, true);
                            auto ErrorLagranj = polynom.GetErrorb();
                            polynom.Newton(lagranj_n, 1e3);
                            auto ErrorNewton = polynom.GetErrorb();
                            cout << "Maximal error for Lagranj polynom: " << ErrorLagranj << endl;
                            cout << "Maximal error for Newton polynom: " << ErrorNewton << endl;
                            if (ErrorLagranj > ErrorNewton)
                            {
                                cout << "As you see, it's more accurate for Newton polynom." << endl;
                            }
                            else if (ErrorLagranj < ErrorNewton) cout << "As you see, it's more accurate for Lagranj polynom." << endl;
                            else if (ErrorLagranj == ErrorNewton) cout << "As you see, they're equal!" << endl;
                            system("PAUSE");
                            system("cls");
                        }
                        break;
                    case 4:
                        cout << "To complete the entire program, enter the number '0', to continue, enter any number: " << endl;
                        cout << "1. Task #4.1 and #4.2" << endl;
                        cout << "2. Task #4.3" << endl;
                        cout << "3. Task #4.4" << endl;
                        cin >> menu;
                        if (menu == 0) return 0;
                        if (menu == 1)
                        {
                            cout << "Please enter n: ";
                            int n;
                            cin >> n;
                            polynom.Trigonometric(n, 100000);
                            polynom.SaveDataFile(gnuplotiostreamFile);
                            system("cls");
                        }
                        if (menu == 2)
                        {
                            if (trigonometric_n < 0) { trigonometric_n = OptimalN("Trigonometric", &polynom, false, true); }
                            else
                            {
                                cout << "Minimal m : " << trigonometric_n << endl;
                            }
                            cout << "This task is still in development mode. See you later!" << endl;
                            system("PAUSE");
                            system("cls");
                        }
                        if (menu == 3)
                        {
                            cout << "This task is still in development mode. See you later!" << endl;
                            system("PAUSE");
                            system("cls");
                        }
                        break;
                    case 5:
                        cout << "This task is still in development mode. See you later!" << endl;
                        system("PAUSE");
                        system("cls");
                        break;
                    case 6:
                        cout << "To complete the entire program, enter the number '0', to continue, enter any number: " << endl;
                        cout << "1. Task #5.1" << endl;
                        cout << "2. Task #5.2" << endl;
                        cout << "3. Task #5.3" << endl;
                        cin >> menu;
                        if (menu == 0) return 0;
                        if (menu == 1)
                        {
                            cout << "Please enter n: ";
                            int n;
                            cin >> n;
                            polynom.Spline(n, 100000);
                            polynom.SaveDataFile(gnuplotiostreamFile);
                            system("cls");
                        }
                        if (menu == 2)
                        {
                            if (spline_n < 0) { spline_n = OptimalN("Spline", &polynom, false, true); }
                            else
                            {
                                cout << "Minimal m : " << spline_n << endl;
                            }
                            system("PAUSE");
                            system("cls");
                        }
                        if (menu == 3)
                        {
                            cout << "This task is still in development mode. See you later!" << endl;
                            system("PAUSE");
                            system("cls");
                        }
                        break;
                    default:
                        return 0;
                        break;
                    }
                }
                break;
            }
            case 2:
            {
                Integration integral(0., 1.);
                cout << "Please, compile MapleIntegrationTest and press Enter!" << endl;
                system("PAUSE");
                system("cls");
                while (1)
                {
                    cout << "To complete the entire program, enter the number '0', to continue, enter any number: " << endl;
                    cout << "1. Task #1." << endl;
                    cout << "2. Task #2." << endl;
                    cout << "3. Task #3." << endl;
                    cout << "4. Task #4." << endl;
                    cout << "5. Task #5." << endl;
                    cout << "6. Task #6." << endl;
                    cout << "10. Go to Lab" << endl;
                    cin >> menu;
                    system("cls");
                    if (menu == 0) return 0;
                    if (menu == 10) break;
                    if (menu == 0) return 0;
                    switch (menu)
                    {
                    case 1:
                    {
                        cout << "To complete the entire program, enter the number '0', to continue, enter any number: " << endl;
                        cout << "1. Task #1.1" << endl;
                        cout << "2. Task #1.2" << endl;
                        cout << "3. Task #1.3" << endl;
                        cin >> menu;
                        if (menu == 0) return 0;
                        if (menu == 1)
                        {
                            cout << "Please enter n: ";
                            int n;
                            cin >> n;

                            integral.Rectangle(n);
                            cout << "Rectangle = " << integral.getResult() << "\t\t";
                            cout << "Error = " << integral.getErrorb() << endl;

                            integral.Trapezoid(n);
                            cout << "Trapezoid = " << integral.getResult() << "\t\t";
                            cout << "Error = " << integral.getErrorb() << endl;

                            cout << "Maple = " << integral.getMapleResult() << endl;
                            system("PAUSE");
                            system("cls");
                        }
                        if (menu == 2)
                        {
                            integral.OptimalN("Rectangle", gnuplotiostreamFile, false, true);
                            integral.OptimalN("Trapezoid", gnuplotiostreamFile, false, true);
                            system("cls");
                        }
                        if (menu == 3)
                        {
                            integral.OptimalN("Rectangle", gnuplotiostreamFile, true);
                            integral.OptimalN("Trapezoid", gnuplotiostreamFile, true);
                            system("PAUSE");
                            system("cls");
                        }
                        break;
                    }
                    case 2:
                    {
                        cout << "To complete the entire program, enter the number '0', to continue, enter any number: " << endl;
                        cout << "1. Task #1.1" << endl;
                        cout << "2. Task #1.2" << endl;
                        cout << "3. Task #1.3" << endl;
                        cin >> menu;
                        if (menu == 0) return 0;
                        if (menu == 1)
                        {
                            cout << "Please enter n: ";
                            int n;
                            cin >> n;

                            integral.Simpson(n);
                            cout << "Simpson = " << integral.getResult() << "\t\t";
                            cout << "Error = " << integral.getErrorb() << endl;

                            cout << "Maple = " << integral.getMapleResult() << endl;
                            system("PAUSE");
                            system("cls");
                        }
                        if (menu == 2)
                        {
                            integral.OptimalN("Simpson", gnuplotiostreamFile, false, true);
                            system("cls");
                        }
                        if (menu == 3)
                        {
                            integral.OptimalN("Simpson", gnuplotiostreamFile, true);
                            system("PAUSE");
                            system("cls");
                        }
                        break;
                    }
                    case 3:
                    {
                        cout << "To complete the entire program, enter the number '0', to continue, enter any number: " << endl;
                        cout << "1. Task #1.1" << endl;
                        cout << "2. Task #1.2" << endl;
                        cout << "2. Task #1.3" << endl;
                        cin >> menu;
                        if (menu == 0) return 0;
                        if (menu == 1)
                        {
                            cout << "Please enter n: ";
                            int n;
                            cin >> n;
                            cout << "Please enter k (0,1): ";
                            double k;
                            cin >> k;
                            integral.Aitken(n, k, "Trapezoid");
                            cout << "Aitken: " << integral.getP() << endl;
                            system("PAUSE");
                            system("cls");
                        }
                        if (menu == 2)
                        {
                            cout << "Please enter n: ";
                            int n;
                            cin >> n;
                            integral.Runge(n, 2 * n, "Trapezoid");
                            cout << "Runge = " << integral.getResult() << endl;
                            cout << "Error = " << integral.getErrorb() << endl;
                            system("PAUSE");
                            system("cls");
                        }
                        if (menu == 3)
                        {
                            cout << "Please enter n: ";
                            int n;
                            cin >> n;
                            cout << "Please enter k (0,1): ";
                            double k;
                            cin >> k;
                            integral.Romberg(n, k, "Trapezoid");
                            cout << "romberg = " << integral.getResult() << endl;
                            cout << "error = " << integral.getErrorb() << endl;
                            system("PAUSE");
                            system("cls");
                        }

                        break;
                    }
                    case 4:
                    {
                        cout << "To complete the entire program, enter the number '0', to continue, enter any number: " << endl;
                        cout << "1. Task #1.1" << endl;
                        cout << "2. Task #1.2" << endl;
                        cin >> menu;
                        if (menu == 0) return 0;
                        if (menu == 1)
                        {
                            cout << "Please enter n: ";
                            int n;
                            cin >> n;

                            integral.Gauss(n);
                            cout << "Gauss = " << integral.getResult() << "\t\t";
                            cout << "Error = " << integral.getErrorb() << endl;

                            cout << "Maple = " << integral.getMapleResult() << endl;
                            system("PAUSE");
                            system("cls");
                        }
                        if (menu == 2)
                        {
                            integral.OptimalN("Gauss", gnuplotiostreamFile, true, false, 2, 1);
                            system("cls");
                        }

                        break;
                    }
                    case 5:
                    {
                        cout << "KEK" << endl;
                        system("PAUSE");
                        system("cls");
                        break;
                    }
                    case 6:
                    {
                        cout << "To complete the entire program, enter the number '0', to continue, enter any number: " << endl;
                        cout << "1. Task #1.1" << endl;
                        cout << "2. Task #1.2" << endl;
                        cin >> menu;
                        if (menu == 0) return 0;
                        if (menu == 1)
                        {
                            cout << "Please enter n: ";
                            int n;
                            cin >> n;

                            integral.MonteCarlo(n);
                            cout << "MonteCarlo = " << integral.getResult() << "\t\t";
                            cout << "Error = " << integral.getErrorb() << endl;

                            cout << "Maple = " << integral.getMapleResult() << endl;
                            system("PAUSE");
                            system("cls");
                        }
                        if (menu == 2)
                        {
                            cout << "KEK" << endl;
                        }
                        break;
                    }
                    default:
                        break;
                    }
                }
            }
            break;
            case 3:
            {
                system("cls");
                while (1)
                {
                    cout << "To complete the entire program, enter the number '0', to continue, enter any number: " << endl;
                    cout << "1. Task #1." << endl;
                    cout << "2. Task #2." << endl;
                    cout << "3. Task #3." << endl;
                    cout << "4. Task #4." << endl;
                    cout << "10. Go to Lab" << endl;
                    cin >> menu;
                    system("cls");
                    if (menu == 0) return 0;
                    if (menu == 10) break;
                    switch (menu)
                    {
                    case 1:
                    {
                        cout << "To complete the entire program, enter the number '0', to continue, enter any number: " << endl;
                        cout << "1. Task #1.1" << endl;
                        cout << "2. Task #1.2" << endl;
                        cout << "3. Task #1.3" << endl;
                        cin >> menu;
                        if (menu == 0) return 0;
                        if (menu == 1)
                        {
                            cout << "Please enter n: ";
                            int n;
                            cin >> n;
                            system("PAUSE");
                            system("cls");
                        }
                        if (menu == 2)
                        {
                            system("cls");
                        }
                        if (menu == 3)
                        {
                            system("cls");
                        }
                        break;
                    }
                    case 2:
                    {
                        cout << "To complete the entire program, enter the number '0', to continue, enter any number: " << endl;
                        cout << "1. Task #1.1" << endl;
                        cout << "2. Task #1.2" << endl;
                        cout << "3. Task #1.3" << endl;
                        cin >> menu;
                        if (menu == 0) return 0;
                        if (menu == 1)
                        {
                            cout << "Please enter n: ";
                            int n;
                            cin >> n;
                            SoLE sol(n);
                            sol.LU();
                            system("PAUSE");
                            system("cls");
                        }
                        if (menu == 2)
                        {
                            system("cls");
                        }
                        if (menu == 3)
                        {
                            system("cls");
                        }
                        break;
                    }
                    case 3:
                    {
                        SoLE sol;
                        sol.RationalInterpolation(10, 13, -0.5, 0.5, gnuplotiostreamFile);
                        system("cls");
                    }
                    break;
                    case 4:
                    {
                        break;
                    }
                    default:
                        break;
                    }
                }
            }
            break;
            case 4:
            {
                SNEATS rot;
                system("cls");
                while (1)
                {
                    cout << "To complete the entire program, enter the number '0', to continue, enter any number: " << endl;
                    cout << "1. Task #1." << endl;
                    cout << "2. Task #2." << endl;
                    cout << "3. Task #3." << endl;
                    cout << "10. Go to Lab" << endl;
                    cin >> menu;
                    system("cls");
                    if (menu == 0) return 0;
                    if (menu == 10) break;
                    switch (menu)
                    {
                    case 1:
                    {

                        rot.Roots(0., 2., 100, "Bisection");
                        cout << "Bisection" << endl;
                        rot.PrintRoots();
                        cout << endl;
                        rot.Roots(0., 2., 100, "Secant");
                        cout << "Secant" << endl;
                        rot.PrintRoots();
                        cout << endl;
                        rot.Roots(0., 2., 100, "Secant_n");
                        cout << "Secant_n" << endl;
                        rot.PrintRoots();
                        cout << endl;
                        rot.Roots(0., 2., 10, "FixedPointIteration");
                        cout << "FixedPointIteration" << endl;
                        rot.PrintRoots();
                        cout << endl;
                        rot.Roots(0., 2., 100, "Newton");
                        cout << "Newton" << endl;
                        rot.PrintRoots();
                        cout << endl;
                        system("PAUSE");
                        system("cls");
                        break;
                    }
                    case 2:
                    {
                        break;
                    }
                    case 3:
                        break;
                    case 4:
                    {
                        break;
                    }
                    default:
                        break;
                    }
                }
            }
            break;
            case 5:
                break;
            case 10:
            {cout << R"(Danis
              _.--""`-..
            ,'          `.
          ,'          __  `.
         /|          " __   \
        , |           / |.   .
        |,'          !_.'|   |
      ,'             '   |   |
     /              |`--'|   |
    |                `---'   |
     .   ,                   |                       ,".
      ._     '           _'  |                    , ' \ `
  `.. `.`-...___,...---""    |       __,.        ,`"   L,|
  |, `- .`._        _,-,.'   .  __.-'-. /        .   ,    \
-:..     `. `-..--_.,.<       `"      / `.        `-/ |   .
  `,         """"'     `.              ,'         |   |  ',,
    `.      '            '            /          '    |'. |/
      `.   |              \       _,-'           |       ''
        `._'               \   '"\                .      |
           |                '     \                `._  ,'
           |                 '     \                 .'|
           |                 .      \                | |
           |                 |       L              ,' |
           `                 |       |             /   '
            \                |       |           ,'   /
          ,' \               |  _.._ ,-..___,..-'    ,'
         /     .             .      `!             ,j'
        /       `.          /        .           .'/
       .          `.       /         |        _.'.'
        `.          7`'---'          |------"'_.'
       _,.`,_     _'                ,''-----"'
   _,-_    '       `.     .'      ,\
   -" /`.         _,'     | _  _  _.|
    ""--'---"""""'        `' '! |! /
                            `" " -')";
            cout << endl << R"(Myrat
     __                                _.--'"7
    `. `--._                        ,-'_,-  ,'
     ,'  `-.`-.                   /' .'    ,|
     `.     `. `-     __...___   /  /     - j
       `.     `  `.-""        " .  /       /
         \     /                ` /       /
          \   /                         ,'
          '._'_               ,-'       |
             | \            ,|  |   ...-'
             || `         ,|_|  |   | `             _..__
            /|| |          | |  |   |  \  _,_    .-"     `-.
           | '.-'          |_|_,' __!  | /|  |  /           \
   ,-...___ .=                  ._..'  /`.| ,`,.      _,.._ |
  |   |,.. \     '  `'        ____,  ,' `--','  |    /      |
 ,`-..'  _)  .`-..___,---'_...._/  .'      '-...'   |      /
'.__' ""'      `.,------'"'      ,/            ,     `.._.' `.
  `.             | `--........,-'.            .         \     \
    `-.          .   '.,--""     |           ,'\        |      .
       `.       /     |          L          ,\  .       |  .,---.
         `._   '      |           \        /  .  L      | /   __ `.
            `-.       |            `._   ,    l   .    j |   '  `. .
              |       |               `"' |  .    |   /  '      .' |
              |       |                   j  |    |  /  , `.__,'   |
              `.      L                 _.   `    j ,'-'           |
               |`"---..\._______,...,--' |   |   /|'      /        j
               '       |                 |   .  / |      '        /
                .      .              ____L   \'  j    -',       /
               / `.     .          _,"     \   | /  ,-','      ,'
              /    `.  ,'`-._     /         \  i'.,'_,'      .'
             .       `.      `-..'             |_,-'      _.'
             |         `._      |            ''/      _,-'
             |            '-..._\             `__,.--'
            ,'           ,' `-.._`.            .
           `.    __      |       "'`.          |
             `-"'  `""""'            7         `.
                                    `---'--.,'"`')" << endl;
            cout << endl << R"(Timur
                   _,........_
               _.-'    ___    `-._
            ,-'      ,'   \       `.
 _,...    ,'      ,-'     |  ,""":`._.
/     `--+.   _,.'      _.',',|"|  ` \`
\_         `"'     _,-"'  | / `-'   l L\
  `"---.._      ,-"       | l       | | |
      /   `.   |          ' `.     ,' ; |
     j     |   |           `._`"""' ,'  |__
     |      `--'____          `----'    .' `.
     |    _,-"""    `-.                 |    \
     l   /             `.               F     l
      `./     __..._     `.           ,'      |
        |  ,-"      `.    | ._     _.'        |
        . j           \   j   /`"""      __   |          ,"`.
         `|           | _,.__ |        ,'  `. |          |   |
          `-._       /-'     `L       .     , '          |   |
              F-...-'          `      |    , /           |   |
              |            ,----.     `...' /            |   |
              .--.        j      l        ,'             |   j
             j    L       |      |'-...--<               .  /
             `     |       . __,,_    ..  |               \/
              `-..'.._  __,-'     \  |  |/`._           ,'`
                  |   ""       .--`. `--,  ,-`..____..,'   |
                   L          /     \ _.  |   | \  .-.\    j
                  .'._        l     .\    `---' |  |  || ,'
                   .  `..____,-.._.'  `._       |  `--;"I'
                    `--"' `.            ,`-..._/__,.-1,'
                            `-.__  __,.'     ,' ,' _-'
                                 `'...___..`'--^--' )" << endl;
            system("PAUSE");
            system("cls");
            }
            break;
            case 0:
                return 1;
                break;
            default:
                break;
            }

        }
        return 1;
    }
}


int main()
{
    setlocale(LC_ALL, "Russian");
    //GetMac(); // Верификация пользователя
    auto p = Menu();
    return p;
    /*ISMSofLAE ISM;*/
   /* SoLE sol;
    cout << "Please enter n: ";
    int n;
    cin >> n;
    sol.diag(n);
    system("PAUSE");
    system("cls");*/
    /*SNEATS S;
    S.FixedPointIterationSystem(0.9, 0.5);
    S.PrintSystemRoots();*/
    //ISMSofLAE ISM;
    //return 0;
}

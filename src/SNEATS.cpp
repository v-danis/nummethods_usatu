#include "SNEATS.h"

SNEATS::SNEATS()
{
	this->f["Bisection"] = [&](auto a, auto b) {Bisection(a, b); };
	this->f["Secant"] = [&](auto a, auto b) {Secant(a, b); };
	this->f["Secant_n"] = [&](auto a, auto b) {Secant_n(a, b); };
	this->f["FixedPointIteration"] = [&](auto a, auto b) {FixedPointIteration(a, b); };
	this->f["Newton"] = [&](auto a, auto b) {Newton(a, b); };
}
// ����� �������� -------------------------------------------------------------------------------------
void SNEATS::Bisection(double a, double b)
{
	int it = 0;
	while (b - a > Delta) // ���� �� ����� ���������� ����������� ��������
	{
		if (y(a) * y((b + a) / 2.) == 0.)
			break;
		else if (y(a) * y((b + a) / 2.) > 0.)
			a = (b + a) / 2.;
		else
			b = (b + a) / 2.;
		it++;
	}
	this->roots.push_back(make_pair(it, (b + a) / 2.));
}

// -----------------------------------------------------------------------------------------------------
// ����� ���� ------------------------------------------------------------------------------------------
void SNEATS::Secant(double a, double b)
{
	double t;
	int it = 0;
	while (fabs(b - a) >= Delta) {
		it++;
		t = a + (y(b) * (b - a)) / (y(b) - y(a));
		if (y(a) * y(t) < 0) {
			b = t;
		}
		else if (y(t) * y(b) < 0) {
			a = t;
		}
		else
		{
			this->roots.push_back(make_pair(it,t));
			break;
		}
	}
	this->roots.push_back(make_pair(it, t));

}

// -----------------------------------------------------------------------------------------------------
// ����� ������� �������� ------------------------------------------------------------------------------
void SNEATS::FixedPointIteration(double a, double b)
{
	double rez; int it = 0;
	do {
		rez = a;
		a = fi(a);
		it++;
	} while (fabs(rez - a) > Delta );
	this->roots.push_back(make_pair(it, a));
}

// -----------------------------------------------------------------------------------------------------
// ����� ����������� -----------------------------------------------------------------------------------
void SNEATS::Newton(double a, double b)
{
	double x_next = 0;
	double x_curr = a;
	int it = 0;

	while (fabs(y(x_curr) / df(x_curr)) >= Delta)
	{
		it++;
		x_next = x_curr - y(x_curr) / df(x_curr);
		x_curr = x_next;
	}

	this->roots.push_back(make_pair(it, x_next));
}

// -----------------------------------------------------------------------------------------------------
// ����� ������� ---------------------------------------------------------------------------------------
void SNEATS::Secant_n(double a, double b)
{
	double x_next = 0;
	double x_prev = a;
	double x_curr = b;
	double tmp;
	int it = 0;
	do
	{
		it++;
		tmp = x_next;
		x_next = x_curr - y(x_curr) * (x_curr - x_prev) / (y(x_curr) - y(x_prev));
		x_prev = x_curr;
		x_curr = tmp;
	} while (abs(x_next - x_curr) > Delta);

	this->roots.push_back(make_pair(it, x_next));
}

// -----------------------------------------------------------------------------------------------------
void SNEATS::FixedPointIterationSystem(double x_0, double y_0)
{
	double rez;
	double x_k = x_0, y_k = y_0;
	int it = 0;
	do {
		//cout << "x [" << it << "] = " << setw(8) << left << x_k << "\ty [" << it << "] = " << setw(8) << left << y_k << endl;
		rez = max(fabs(f_1(x_k, y_k) - x_k), fabs(f_2(x_k, y_k) - y_k));
		double temp = x_k;
		x_k = f_1(temp, y_k);
		y_k = f_2(x_k, y_k);
		it++;
	} while (rez > Delta);
	this->system_roots = make_pair(it, make_pair(x_k, y_k));
}
// -----------------------------------------------------------------------------------------------------
void SNEATS::Roots(double a, double b,int n, string name)
{
	methods::iterator x = this->f.find(name);
	if (x == f.end()) // ���� ������ ����� ���
	{
		cout << "�an't find such a function, check the request!" << endl;
		system("PAUSE");
		return;
	}
	this->roots.clear();
	double h = (b - a) / (n - 1);
	for (int i = 0; i < n; i++)
	{
		double a_ = a + i * h;
		double b_ = a_ + h;
		if (y(a_) * y(b_) <= 0.)
		{
			(x->second)(a_,b_);
		}
	}
}

void SNEATS::PrintRoots()
{
	int i = 0;
	for (auto &a: roots)
	{
		i++;
		cout << "x [" << i << "] = " << setw(8) << a.second << "\titerations = " << a.first << endl;
	}
}

void SNEATS::PrintSystemRoots()
{
	cout << "x = " << system_roots.second.first << "\ty = " << system_roots.second.second << "\titerations = " << system_roots.first << endl;
}

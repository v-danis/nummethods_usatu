#include "SoLE.h"

SoLE::SoLE()
{
    this->n = 5;
    this->ReadFromFile();
    auto gen = default_random_engine();
    auto dis = uniform_real_distribution<>(0., 10.);
    matrix.resize(n);
    for (int i = 0; i < n; i++)
    {
        matrix[i].resize(n + 1);
        for (int j = 0; j < this->matrix[i].size(); j++)
        {
            this->matrix[i][j] = dis(gen);
        }
    }
}

SoLE::SoLE(int n) :n(n)
{
    this->ReadFromFile();
    auto gen = default_random_engine();
    auto dis = uniform_real_distribution<>(0., 10.);
    matrix.resize(n);
    for (int i = 0; i < this->n; i++)
    {
        matrix[i].resize(this->n + 1);
        for (int j = 0; j < this->matrix[i].size(); j++)
        {
            this->matrix[i][j] = dis(gen);
        }
    }
}

bool SoLE::ReadFromFile()
{
    ifstream f;
    f.open("SoLE.txt");
    if (!f)
    {
        return false;
    }
    double x, y;
    while (f >> x >> y)
    {
        XY.push_back({ x,y });
    }
    f.close();
    return true;
}

void SoLE::PrintMatrix()
{
    for (auto& x : matrix)
    {
        cout << fixed << setprecision
        (6);
        copy(x.begin(), x.end(), ostream_iterator<double>(cout, "  "));
        cout << endl;
    }
}

void SoLE::SaveDataFile(string gnuplotiostreamFile, vector<vector<double>> data, vector<vector<double>> input)
{
    Gnuplot gp(gnuplotiostreamFile); // ������ � Gnuplot
    gp << " set title 'Interpolation'\n";
    // ������ ���������� ������ � ����� data.dat � output.dat 
    gp << "set pointsize 1\n"
        << "plot" << gp.file1d(data, "RationalInterpolation.dat") << "with lines title 'RationalInterpolation',"
        << gp.file1d(input, "RationalInterpolationFunc.dat") << "with lines title 'Initial function'," << endl;
    system("PAUSE");
}

void SoLE::Gauss(vector<vector<double>> A, vector<double> B)
{
    size_t rows = A.size();
    size_t cols = A[0].size();

    double temp;

    // �� ������ ������ � ������� �������� ��������:
    // ������������ ����� ���, ����� �� ��������� ��� ���������� �������
    // � ��������� ��������� ���� ������� ���������
    for (size_t k = 0; k < rows; k++) // ������� �������
    {
        // ����� ������ � ������������ ��������� � ������� �������
        size_t p = k;

        // ���������� ������ ������� ��������� �������
        temp = A[k][k];

        // ����� ������������� �� ������ ��������
        for (size_t i = k + 1; i < rows; i++)
        {
            if (abs(A[i][k]) > abs(temp))
            {
                temp = A[i][k];
                p = i;
            }
        }

        // ���� ���� ������� ������ � ������� ���������, �� ������������ � �� k-�� �����
        if (p != k)
        {
            for (size_t i = k; i < cols; i++)
            {
                temp = A[k][i];
                A[k][i] = A[p][i];
                A[p][i] = temp;
            }

            temp = B[k];
            B[k] = B[p];
            B[p] = temp;
        }

        // �������� ��� ���� k-���
        for (size_t i = k + 1; i < rows; i++)
        {
            temp = A[i][k] / A[k][k];

            // �������� i-�� ������
            for (size_t j = k; j < cols; j++)
            {
                A[i][j] -= A[k][j] * temp;
            }

            B[i] -= B[k] * temp;
        }
    }

    vector<double> solution(cols);

    // ������ ��������� ���������� ���������� ��������
    for (size_t i = rows; i < cols; i++)
    {
        solution[i] = 1;
    }

    // ������� x[k] ������� ����������� ����� ����� �� �������
    for (size_t k = rows; k-- > 0; )
    {
        // ���� ����������� ��� "����" = 0, �� ����� ����� ����� ��������,
        // �.�. �� ���� ������ �� �������
        // ������ ������������ ������� �� ���������������
        if (A[k][k] == 0.0)
        {
            solution[k] = rand();
        }
        else
        {
            // �������� �� ������� ��������� ������ ��������� "����" ���� �������
            solution[k] = B[k];
            for (size_t l = k + 1; l < cols; l++)
            {
                solution[k] -= A[k][l] * solution[l];
            }
            solution[k] /= A[k][k];
        }
    }
    this->root = solution;
}

void SoLE::LU()
{
    int n = this->matrix.size();
    vector<vector<double>> L(n);
    vector<vector<double>> U(n);
    vector<vector<double>> U_inv(n);
    vector<double> b(n);
    vector<double> y(n);
    root.clear();

    for (int i = 0; i < n; i++)
    {
        L[i].resize(n + 1);
        U[i].resize(n + 1);
        U_inv[i].resize(n);
        for (int j = 0; j < n + 1; j++)
        {
            U[i][j] = this->matrix[i][j];
        }
    }

    for (int k = 0; k < n; k++)
    {
        int j_max = k;
        for (int j = k; j < n; j++)
            if (fabs(U[j_max][k]) < fabs(U[j][k]))
                j_max = j;

        //������������ ������
        for (int i = 0; i < n + 1; i++)
        {
            double t_1 = U[k][i];
            U[k][i] = U[j_max][i];
            U[j_max][i] = t_1;

            double t_2 = this->matrix[k][i];
            this->matrix[k][i] = this->matrix[j_max][i];
            this->matrix[j_max][i] = t_2;
        }

        //���� ������������ ������� �������
        if (U[j_max][k] == 0)
        {
            int count = 0;
            for (int i = 0; i < n; i++)
            {
                if (U[k][i] == 0)
                    count++;
                if (count == n && U[k][n] == 0)//���� �����. � ������ = 0 � ����. ���� = 0
                {
                    throw std::exception("SLAE has many solutions");
                }
                else if (count == n && U[k][n] != 0)//���� �����. � ������ = 0 � ����. ���� �� ����� 0
                {
                    throw std::exception("SLAE has no solutions");
                }
            }
        }

        //���������� ����������
        //for (int j = n; j >= k; j--)
        //    U[k][j] = U[k][j] / U[k][k];//����� �� �������. a[1][1],a[2][2] � �.�.

        for (int i = k + 1; i < n; i++)
            for (int j = n; j >= k; j--)
                U[i][j] = U[i][j] - U[i][k] * U[k][j] / U[k][k];//�������� �� 2 ��-�� 1-�� �����. �� �����. � �.�. */

    }

    for (int i = 0; i < n; i++)
    {
        b[i] = this->matrix[i][n];
        for (int j = 0; j < n; j++)
        {
            U_inv[i][j] = U[i][j];
        }
    }

    Matrix::Inversion(U_inv);
    L = Matrix::Multiplication(this->matrix, U_inv);

    //std::cout << "Matrix A:" << std::endl;
    //for (int i = 0; i < n; i++)
    //{
    //    for (int j = 0; j < n; j++)
    //    {
    //        std::cout << A[i][j] << "\t";
    //    }
    //    std::cout << std::endl;
    //}
    //std::cout << "-------------------------------------------------" << std::endl;

    //std::cout << "Matrix U:" << std::endl;
    //for (int i = 0; i < n; i++)
    //{
    //    for (int j = 0; j < n; j++)
    //    {
    //        std::cout << U[i][j] << "\t";
    //    }
    //    std::cout << std::endl;
    //}

    //std::cout << "Matrix L:" << std::endl;
    //for (int i = 0; i < n; i++)
    //{
    //    for (int j = 0; j < n; j++)
    //    {
    //        std::cout << L[i][j] << "\t";
    //    }
    //std::cout << std::endl;
    //}
    //std::cout << "-------------------------------------------------" << std::endl;

    //������ ���
    for (int i = 0; i < n; i++)
    {
        y[i] = b[i];
        for (int j = 0; j < i; j++)
        {
            y[i] -= L[i][j] * y[j];
        }
    }

    //�������� ���
    for (int i = n - 1; i >= 0; i--)
    {
        root[i] = y[i];
        for (int j = n - 1; j > i; j--)
        {
            root[i] = root[i] - root[j] * U[i][j];
        }
        root[i] = root[i] / U[i][i];
    }

    std::cout << "root:" << std::endl;
    for (int i = 0; i < n; i++)
        std::cout << "x" << i + 1 << " = " << root[i] << std::endl;

}

void SoLE::RationalInterpolation(int p, int q, double a, double b, string gnuplotiostreamFile)
{
    this->p = p;
    this->q = q;
    int n = p + q + 1;
    double delta = (b - a) / (double)(n - 1); // ��� ������� ����� ������ ������������
    vector<double> x_arr;
    vector<double> y_arr;
    vector<vector<double>> input;
    for (double i = a; i <= b; i += delta)
    {
        x_arr.push_back(i);
        y_arr.push_back(func(i));
        input.push_back({ i, func(i) });
    }
    vector<double> B(n);
    this->matrix.clear();
    this->matrix.resize(n);
    for (int i = 0; i < n; i++)
    {
        // ������������� n + 1 = p + q + 2
        this->matrix[i].resize(n + 1);
    }
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j <= p; j++)
        {
            this->matrix[i][j] = pow(x_arr[i], j);
        }
        for (int j = 0; j <= q; j++)
        {
            this->matrix[i][j + p + 1] = -y_arr[i] * pow(x_arr[i], j);
        }
        B[i] = 0;
    }

    Gauss(this->matrix, B);
    vector<vector<double>> Result;
    for (auto it = x_arr.begin(); it < x_arr.end(); it++)
    {
        Result.push_back({ *it, RationalValue(*it) });
    }
    SaveDataFile(gnuplotiostreamFile, Result, input);
}

double SoLE::RationalValue(double x)
{
    double numeratorVal = 0.0;
    double denominatorVal = 0.0;

    for (int i = 0; i <= p; i++)
    {
        numeratorVal += this->root[i] * pow(x, i);
    }

    for (int i = 0; i <= q; i++)
    {
        denominatorVal += this->root[i + p + 1] * pow(x, i);
    }
    return numeratorVal / denominatorVal;
}

void SoLE::createMatrix(int N)
{
    auto x_dist = std::uniform_real_distribution<>(1., 8.);
    auto rng = std::default_random_engine();
    this->matrix.clear();
    this->matrix.resize(N + 1);
    for (int i = 0; i <= N; i++)
    {
        this->matrix[i].resize(N + 2);
        for (int j = 0; j <= N; j++)
        {
            matrix[i][j] = 0;
        }
        matrix[i][N + 1] = x_dist(rng);
    }

    for (int i = 2; i <= N; i++)
        matrix[i][i - 2] = (double)x_dist(rng); //ai

    for (int i = 1; i <= N; i++)
        matrix[i][i - 1] = (double)x_dist(rng); //bi

    for (int i = 0; i < N; i++)
        matrix[i][i + 1] = (double)x_dist(rng); //di

    for (int i = 0; i < N - 1; i++)
        matrix[i][i + 2] = (double)x_dist(rng); //ei

    for (int i = 2; i <= N - 2; i++)
        matrix[i][i] = fabs(matrix[i][i - 2]) + fabs(matrix[i][i - 1]) + fabs(matrix[i][i + 1]) + fabs(matrix[i][i + 2]) + 1.;

    matrix[0][0] = fabs(matrix[0][1]) + fabs(matrix[0][2]) + 1;
    matrix[N][N] = fabs(matrix[N][N - 2]) + fabs(matrix[N][N - 1]) + 1;
    matrix[1][1] = fabs(matrix[1][0]) + fabs(matrix[1][2]) + fabs(matrix[1][3]) + 1;
    matrix[N - 1][N - 1] = fabs(matrix[N - 1][N - 3]) + fabs(matrix[N - 1][N - 2]) + fabs(matrix[N - 1][N]) + 1;

    PrintMatrix();
    cout << endl;
}

void SoLE::diag(int N)
{
    this->root.clear();
    createMatrix(N);
    int n = matrix.size() - 1;
    vector<double> alpha(n + 1, 0);
    vector<double> betta(n, 0);
    vector<double> gamma(n + 2, 0);
    vector<double> F(n + 1, 0);

    for (int i = 0; i < matrix.size(); i++)
    {
        F[i] = matrix[i][matrix.size()];
    }

    vector<double> a(n + 1, 0);
    vector<double> b(n + 1, 0);
    vector<double> c(n + 1, 0);
    vector<double> d(n + 1, 0);
    vector<double> e(n + 1, 0);


    for (int i = 2; i <= n; i++)
        a[i] = matrix[i][i - 2];

    for (int i = 1; i <= n; i++)
        b[i] = -matrix[i][i - 1];

    for (int i = 0; i <= n; i++)
        c[i] = matrix[i][i];

    for (int i = 0; i <= n - 1; i++)
        d[i] = -matrix[i][i + 1];

    for (int i = 0; i <= n - 2; i++)
        e[i] = matrix[i][i + 2];

    alpha[1] = d[0] / c[0];
    betta[1] = e[0] / c[0];
    gamma[1] = F[0] / c[0];

    alpha[2] = (d[1] - betta[1] * b[1]) / (c[1] - b[1] * alpha[1]);
    betta[2] = e[1] / (c[1] - b[1] * alpha[1]);
    gamma[2] = (F[1] + b[1] * gamma[1]) / (c[1] - b[1] * alpha[1]);

    for (int i = 2; i <= n - 2; i++)
    {
        alpha[i + 1] = (d[i] + betta[i] * (a[i] * alpha[i - 1] - b[i]))
            / (c[i] - a[i] * betta[i - 1] + alpha[i] * (a[i] * alpha[i - 1] - b[i]));
        betta[i + 1] = e[i] / (c[i] - a[i] * betta[i - 1] + alpha[i] * (a[i] * alpha[i - 1] - b[i]));
    }
    int i = n - 1;
    alpha[i + 1] = (d[i] + betta[i] * (a[i] * alpha[i - 1] - b[i]))
        / (c[i] - a[i] * betta[i - 1] + alpha[i] * (a[i] * alpha[i - 1] - b[i]));

    for (int i = 2; i <= n; i++)
        gamma[i + 1] = (F[i] - a[i] * gamma[i - 1] - gamma[i] * (a[i] * alpha[i - 1] - b[i]))
        / (c[i] - a[i] * betta[i - 1] + alpha[i] * (a[i] * alpha[i - 1] - b[i]));

    //copy(alpha.begin(), alpha.end(), ostream_iterator<double>(cout, "  "));
    //cout << endl;
    //copy(betta.begin(), betta.end(), ostream_iterator<double>(cout, "  "));
    //cout << endl;
    //copy(gamma.begin(), gamma.end(), ostream_iterator<double>(cout, "  "));
    //cout << endl;
    //copy(F.begin(), F.end(), ostream_iterator<double>(cout, "  "));
    //cout << endl;
    this->root.resize(n + 1);
    //this->root[root.size() - 1] = 0.571749;
    //this->root[root.size() - 2] = 0.0208962;
    this->root[n] = gamma[n + 1];
    this->root[n - 1] = alpha[n] * this->root[n] + gamma[n];
    for (int i = n - 2; i >= 0; i--)
    {
        this->root[i] = alpha[i + 1] * this->root[i + 1] - betta[i + 1] * this->root[i + 2] + gamma[i + 1];
    }
    copy(root.begin(), root.end(), ostream_iterator<double>(cout, "\n"));
    cout << endl;
    vector<double> res = Matrix::VectorMultiplication(this->matrix, this->root);
    copy(res.begin(), res.end(), ostream_iterator<double>(cout, "\n"));
}
#include "Interpolation.h"

Interpolation::Interpolation(double a, double b): a(a), b(b) {} 

Interpolation::Interpolation(){
    a = -0.5;
    b = 0.5;
}

void Interpolation::SaveDataFile(string gnuplotiostreamFile){
    double A, B;
    // ������ ������ � ����� ������� ��� ��������� ������������������ ������������
    if (bull) { A = -0.1; B = 2 * M_PI; } 
    else { A = this->a; B = this->b; }

    // ����� ������������ � ������������� ��������� ��� ��������������� �������
    
    double y_max = *max_element(errors_vect.begin(), errors_vect.end());
    double y_min = *min_element(errors_vect.begin(), errors_vect.end());

    Gnuplot gp(gnuplotiostreamFile); // ������ � Gnuplot
    gp << " set title 'Interpolation'\n";
    // ������ ���������� ������ � ����� data.dat � output.dat 
    gp << "set pointsize 1\n" 
        << "set xrange[" << 1.1 * A << ":" << 1.1 * B << "]\n" 
        << "set yrange [" << 1.3 * y_min << ":" << 1.3 * y_max << "]\n"
        << "plot" << gp.file1d(data, "data.dat") << "with linespoints pointtype 7 title 'Original function', "
        << gp.file1d(output, "output.dat") << "with lines title 'Approximate function'," << endl;
    system("PAUSE");
}

void Interpolation::ErrorGraphics(string gnuplotiostreamFile, vector<vector<double>> errorsVec){
    // ����������� ������ 
    double y_min = errorsVec[0][1];
    for (int i = 0; i < errorsVec.size(); i++)
    {
        if (errorsVec[i][1] < y_min)
        {
            y_min = errorsVec[i][1]; 
        }
    }
    // ������������ ������
    double y_max = errorsVec[0][1];
    for (int i = 0; i < errorsVec.size(); i++)
    {
        if (errorsVec[i][1] > y_max)
        {
            y_max = errorsVec[i][1]; 
        }
    }
    Gnuplot gp(gnuplotiostreamFile); // ������ � Gnuplot
    gp << " set title 'Errors graphic'\n";
    gp << "set pointsize 1\n"
        << "set xrange[0:16]\n"
        << "set xtics 1\n"
        << "set yrange [" << -1000000 * y_min << ":" << 1.1 * y_max << "]\n"
        << "plot" << gp.file1d(errorsVec, "data/errorsVec.dat") << "with linespoints pointtype 7 title 'Errors', "
        << endl;
    system("PAUSE");
}

void Interpolation::ComputeError(){ 
    vector<double> buf_difference; // ������ ��������� ����: y(x) - Ln(x)
    for (int i = 0; i < errors_vect.size(); i++)
    {
        buf_difference.push_back(fabs(y(x_for_pol[i]) - errors_vect[i]));
    }
    // sup|y(x) - Ln(x)|
    auto result = max_element(buf_difference.begin(), buf_difference.end());
    this->errorb = *result;
}

double Interpolation::GetErrorb(){
    return this->errorb;
}

void Interpolation::CreateDataSet(){
    // ������� ���� ��������
    this->x_for_pol.clear();
    this->data.clear();
    this->x_arr.clear();
    this->y_arr.clear();
    this->delta = (double)(b - a) / (double)this->N; // ��� ������� ����� ������������ �������
    for (double i = this->a; i <= this->b; i += this->delta)
    {
        this->x_for_pol.push_back(i);
    }
    this->delta = (this->b - this->a) / (double)this->n; // ��� ������� ����� ������ ������������
    for (double i = this->a; i <= this->b; i += this->delta)
    {
        x_arr.push_back(i);
        y_arr.push_back(y(i));
        this->data.push_back({ i, double(y(i)) });
    }
}

// �������, ����������� ���������������� ��������� ��������

void Interpolation::CreateDataSetLagranj(){
    if (flag) // ����������� ����� 
    {
        CreateDataSet();
    }
    else // ������������� �����
    {
        // ������� ���� ��������
        this->x_for_pol.clear();
        this->data.clear();
        this->x_arr.clear();
        this->y_arr.clear();
        for (double i = 0; i <= (double)(n); i++)
        {
            double x = (b + a) / 2.0 + ((b - a) / 2.0) * cos((M_PI * (2.0 * i + 1.0)) / (2.0 * (double)n + 1));
            x_arr.push_back(x);
            y_arr.push_back(y(x));
            this->data.push_back({ x, double(y(x)) });
        }
        for (double i = 0; i <= (double)(N); i++)
        {
            double x = (b + a) / 2.0 + ((b - a) / 2.0) * cos((M_PI * (2.0 * i + 1.0)) / (2.0 * (double)N + 1));
            this->x_for_pol.push_back(x);
        }
    }
}

void Interpolation::LagranjCalculation(double x, vector<vector<double>>& output){
    double l = 1.0; // ������������
    double L = 0.0; // �����
    for (int i = 0; i < y_arr.size(); i++)
    {
        l = 1.0;
        for (int j = 0; j < x_arr.size(); j++)
        {
            if (j != i)
            {
                l *= (x - x_arr[j]) / (x_arr[i] - x_arr[j]);
            }
        }
        L += y_arr[i] * l;
    }
    output.push_back({ x,L });
}

void Interpolation::Lagranj(int n, int N, bool flag){
    // ��������� ����� �������
    this->n = n;
    this->N = N;
    this->flag = flag;
    CreateDataSetLagranj();
    this->output.clear(); // ������� ������� ����������� �����
    for (int k = 0; k < x_for_pol.size(); k++)
    {
        LagranjCalculation(x_for_pol[k],this->output);
    }
    errors_vect.clear(); // ������� ������� ������
    for (int i = 0; i < output.size(); i++)
    {
        errors_vect.push_back(output[i][1]);
    }
    ComputeError();
}

// �������, ����������� ���������������� ��������� �������

void Interpolation::CreateDataSetNewton(vector<vector<double>> &finite_differences){
    CreateDataSet();
    // ��������� ������ ����������� ��������� �������� �� 1 �� n
    finite_differences.resize(y_arr.size()); 
    finite_differences[0] = y_arr;
    for (int i = 1; i < y_arr.size(); i++)
    {
        finite_differences[i].resize(y_arr.size() - i);
        for (int j = 0; j < y_arr.size() - i; j++)
        {
            finite_differences[i][j] = (finite_differences[i - 1][j + 1] - finite_differences[i - 1][j]) / 
                (x_arr[j + i] - x_arr[j]);
        }
    }
}

void Interpolation::NewtonCalculation(double x,  const vector<vector<double>>& finite_differences, vector<vector<double>>& output){
    double l = 1.0; // ������������
    double L = 0.0; // �����
    int j, i;
    for (i = 0; i < y_arr.size(); i++)
    {
        l = 1.0;
        for (j = 0; j < i; j++)
            l *= (x - x_arr[j]);

        l *= finite_differences[j][0];
        L += l;
    }
    output.push_back({ x,L });
}

void Interpolation::Newton(int n, int N, bool flag){
    // ��������� ����� �������
    this->n = n;
    this->N = N;
    vector<vector<double>> finite_differences; // ��������� ������ ����������� ��������� �������� �� 1 �� n
    CreateDataSetNewton(finite_differences);
    this->output.clear(); // ������� ������� �������� ������
    for (int k = 0; k < x_for_pol.size(); k++)
    {
        NewtonCalculation(x_for_pol[k], finite_differences, output);
    }
    errors_vect.clear(); // ������� ������� ������
    for (int i = 0; i < output.size(); i++)
    {
        errors_vect.push_back(output[i][1]);
    }
    ComputeError();
}

// �������, ����������� ������������������ ������������

void Interpolation::CreateDataSetTrigonometric(vector<double>& A_arr, vector<double>& B_arr){
    // ������� ���� ��������
    this->data.clear();
    this->t_arr.clear();
    this->y_arr.clear();
    this->x_for_pol.clear();
    // ��������� �������� ��������
    this->t_arr.resize(2 * n + 2);
    this->y_arr.resize(2 * n + 2);
    for (int i = 1; i <= (2 * n + 1); i++)
    {
        this->t_arr[i] = (2.0 * M_PI * (i - 1)) / (2.0 * n + 1.0);
        this->y_arr[i] = y(yTrig(t_arr[i], this->a, this->b));
        this->data.push_back({ t_arr[i], y_arr[i] });
    }
    int i = 1;
    do 
    {
        this->x_for_pol.push_back((2.0 * M_PI * (i - 1)) / (2.0 * N + 1.0));
        i++;
    } while (x_for_pol.back() < t_arr.back());
    /*for (int i = 1; i <= (2 * N + 1); i++)
    {
        this->x_for_pol.push_back((2.0 * M_PI * (i - 1)) / (2.0 * N + 1.0));
    }*/
    // ���������� ������������� A_k
    for (int i = 1; i < y_arr.size(); i++)
    {
        A_arr[0] += y_arr[i];
    }
    A_arr[0] *= (1.0 / (2.0 * n + 1.0));
    for (int k = 1; k < A_arr.size(); k++)
    {
        for (int j = 1; j < y_arr.size(); j++)
        {
            //A_arr[k] += y_arr[j] * cos((2.0 * M_PI * k * j)) / (2.0 * n + 1.0);
            A_arr[k] += y_arr[j] * cos(k * t_arr[j]);
        }
        A_arr[k] *= (2.0 / (2.0 * n + 1.0));
    }
    // ���������� ������������� B_k
    for (int k = 1; k < B_arr.size(); k++)
    {
        for (int j = 1; j < y_arr.size(); j++)
        {
            //B_arr[k] += y_arr[j] * sin((2.0 * M_PI * k * j) / (2.0 * n + 1.0));
            B_arr[k] += y_arr[j] * sin(k * t_arr[j]);
        }
        B_arr[k] *= (2.0 / (2.0 * n + 1.0));
    }
}

void Interpolation::TrigonometricCalculation( double t, const vector<double>& A_arr, const vector<double>& B_arr, vector<vector<double>>& output){
    double P = A_arr[0];
    for (int k = 1; k < A_arr.size(); k++)
    {
        P += (A_arr[k] * cos(k * t) + B_arr[k] * sin(k * t));
    }
    output.push_back({ t,P });
}

void Interpolation::Trigonometric(int n, int N, bool flag){
    vector<double> A_arr; // ������ ������������� A_k
    vector<double> B_arr; // ������ ������������� B_k   
    // ��������� ����� �������
    this->bull = flag;
    this->n = n;
    this->N = N;
    // ��������� �������� ��������
    A_arr.resize(n + 1);
    B_arr.resize(n + 1);
    CreateDataSetTrigonometric(A_arr, B_arr);
    this->output.clear(); // ������� ������� �������� ������
    for (int i = 0; i < x_for_pol.size(); i++)
    {
        TrigonometricCalculation(x_for_pol[i], A_arr, B_arr, output);
    }
    errors_vect.clear(); // ������� ������� ������
    for (int i = 0; i < output.size(); i++)
    {
        errors_vect.push_back(output[i][1]);
    }
    ComputeError();
}

// �������, ����������� ������������ ���������

void Interpolation::CreateDataSetSpline(vector<double>& A, vector<double>& B, vector<double>& C, vector<double>& D){
    CreateDataSet();
    // ��������� �������� ��������
    A.resize(y_arr.size());
    B.resize(A.size());
    C.resize(A.size() + 1); //c[1] = c[n+1] = 0
    D.resize(A.size());

    // ����� �������� 
    // �������� � ������������ �������� ������������� ��������
    vector<double> a(A.size(), delta);  a[1] = 0;
    vector<double> b(A.size(), -4.0 * delta);
    vector<double> c(A.size(), delta); c[c.size() - 1] = 0;
    vector<double> d(A.size());
    vector<double> alpha(A.size() + 1); alpha[2] = 0;
    vector<double> betta(A.size() + 1); betta[2] = 0;

    for (int i = 2; i < d.size(); i++)
    {
        d[i] = 3.0 * (((y_arr[i] - y_arr[i - 1]) / delta) - ((y_arr[i - 1] - y_arr[i - 2]) / delta));
    }
    for (int i = 2; i < alpha.size() - 1; i++)
    {
        alpha[i + 1] = c[i] / (b[i] - a[i] * alpha[i]);
        betta[i + 1] = (a[i] * betta[i] - d[i]) / (b[i] - a[i] * alpha[i]);
    }

    // ���������� ������������� �������
    for (int i = 1; i < A.size(); i++) { A[i] = y_arr[i - 1]; }

    C[1] = 0;
    C[C.size() - 1] = 0;
    for (int i = C.size() - 2; i >= 2; i--)
    {
        C[i] = alpha[i + 1] * C[i + 1] + betta[i + 1];
    }

    for (int i = 1; i < B.size()-1; i++)
    {
        B[i] = ((y_arr[i] - y_arr[i - 1]) / delta) -
            (delta / 3.0) * (C[i + 1] + 2.0 * C[i]);
        D[i] = (C[i + 1] - C[i]) / (3.0 * delta);
    }
    B[B.size() - 1] = ((y_arr[B.size() - 1] - y_arr[B.size() - 2]) / delta) - (delta * C[B.size() - 1] * 2.0) / 3.0;
    D[B.size() - 1] = -C[B.size() - 1] / (3.0 * delta);
}

void Interpolation::SplineCalculation(double x, const vector<double>& A, const vector<double>& B, const vector<double>& C,
    const vector<double>& D, vector<vector<double>>& output){
    for (int i = 1; i < x_arr.size(); i++)
    {
        if (x >= x_arr[i - 1] && x <= x_arr[i])
        {
            double P = A[i] + B[i] * (x - x_arr[i - 1]) + C[i] * pow((x - x_arr[i - 1]), 2) + D[i] * pow((x - x_arr[i - 1]), 3);
            output.push_back({ x, P });
            return;
        }
    }
}

void Interpolation::Spline(int n, int N, bool flag)
{
    vector<double> A; // ������ ������������� A_k 
    vector<double> B; // ������ ������������� B_k 
    vector<double> C; // ������ ������������� C_k 
    vector<double> D; // ������ ������������� D_k 
    // ��������� ����� �������
    this->n = n;
    this->N = N;
    CreateDataSetSpline(A, B, C, D);
    this->output.clear(); // ������� ������� �������� ������
    for (int i = 0; i < x_for_pol.size(); i++)
    {
        SplineCalculation(x_for_pol[i], A, B, C, D, output);
    }
    errors_vect.clear(); // ������� ������� ������
    for (int i = 0; i < output.size(); i++)
    {
        errors_vect.push_back(output[i][1]);
    }
    ComputeError();
}

// �������, ����������� ��������� ����������� �����������

void Interpolation::BestApproximation(vector<vector<double>>& vec, int n, int N)
{
    if (vec.empty()) 
    {
        this->n = n;
        this->N = N;
        CreateDataSet(); 
        vec = this->data;
    }
    else 
    {
        this->data.clear();
        this->x_arr.clear();
        this->y_arr.clear();
        for (auto& v : vec)
        {
            this->x_arr.push_back(v[0]);
            this->y_arr.push_back(v[1]);
        }
    }
    vector<vector<double>> BA;
    for (int i = 0; i < vec.size() - 1; i++)
    {
        double t = (b + a) / 2.0 + ((b - a) / 2.0) * cos((M_PI * (2.0 * i + 1.0)) / (2.0 * (double)(vec.size() - 2) + 1));
        LagranjCalculation(t, BA);
    }
    vec.clear();
    vec = BA;
}

void Interpolation::Telescope()
{
    double maxDelta = 1;
    vector<vector<double>> vec;
    /*ifstream f;
    f.open("MapleTaylorResult.txt");
    if (!f)
    {
        exit(666);
    }
    double x, y;
    while (f >> x >> y)
    {
        vec.push_back({ x, y });
    }*/
    //for (int i = 0; i < vec.size(); i++) { cout << vec[i][0] << " " << vec[i][1] << endl; }
    vector<double> errors;
    this->n = 15;
    this->N = 100000;
    CreateDataSet();
    for (int i = 0; i < this->x_for_pol.size(); i++)
    {
        vec.push_back({ x_for_pol[i], Taylor(x_for_pol[i]) });
    }
    vector<double> fx;
    while (maxDelta > Delta)
    {
        fx.clear();
        errors.clear();
        BestApproximation(vec);
        for (int i = 0; i < vec.size(); i++)
        {
            fx.push_back(y(x_for_pol[i]));
        }
        for (int i = 0; i < vec.size(); i++)
        {
            errors.push_back(fabs(fx[i] - vec[i][1]));
        }
        maxDelta = *max_element(errors.begin(), errors.end());
        cout << "Delta = " << maxDelta << endl;
        cout << "m: " << vec.size() - 1 << endl;
    }
    //cout << "Minimal m: " << vec.size() - 1 << endl;
}
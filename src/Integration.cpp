#include "Integration.h"

Integration::Integration() 
{
    this->a = -0.5;
    this->b = 0.5;
    this->result = 0.0;
    this->f["Trapezoid"] = [&](auto n) {Trapezoid(n); };
    this->f["Rectangle"] = [&](auto n) {Rectangle(n); };
    this->f["Simpson"] = [&](auto n) {Simpson(n); };
    this->f["Gauss"] = [&](auto n) {Gauss(n); };
    //this->f["Romberg"] = [&](auto n) {Trapezoid(n); };
    this->f["MonteCarlo"] = [&](auto n) {MonteCarlo(n); };
    WriteToFile();
}

Integration::Integration(double a, double b)
{
    this->a = a;
    this->b = b;
    this->result = 0.0;
    this->f["Trapezoid"] = [&](auto n) {Trapezoid(n); };
    this->f["Rectangle"] = [&](auto n) {Rectangle(n); };
    this->f["Simpson"] = [&](auto n) {Simpson(n); };
    this->f["Gauss"] = [&](auto n) {Gauss(n); };
    //this->f["Romberg"] = [&](auto n) {Trapezoid(n); };
    this->f["MonteCarlo"] = [&](auto n) {MonteCarlo(n); };
    WriteToFile();
}

void Integration::WriteToFile()
{
    ofstream file;
    file.open("SegmentAB.txt");
    file << this->a << "\n" << this->b;
    file.close();
}

bool Integration::ReadFromFile()
{
    ifstream f;
    f.open("D:/dev/NumMethods_usatu/data/MapleResult.txt");
    if (!f)
    {
        return false;
    }
    f >> this->mapleResult;
    f.close();
    return true;
}

void Integration::ComputeError()
{
    if (ReadFromFile())
    {
        this->errorb = fabs(this->result - this->mapleResult);
    }
    else cout << "Can't compute error! (���. ��������!)" << endl;
}

void Integration::OptimalN(string name,string gnuplotiostreamFile,bool limit, bool graphics, int start, int steps, int end)
{
    func::iterator x = this->f.find(name);
    if (x == f.end()) // ���� ������ ����� ���
    {
        cout << "�an't find such a function, check the request!" << endl;
        system("PAUSE");
        return;
    }
    vector<vector<double>> errorsVec; // ������ ������ �����������: |J - I|
    if (!limit)
    {
        for (start; start <= end; start = start + steps)
        {
            (x->second)(start);
            errorsVec.push_back({ (double)start, this->errorb });
        }
        double min = errorsVec[0][1]; // ����������� ������
        int idx; // ����������� �������� n
        for (int i = 0; i < errorsVec.size(); i++)
        {
            if (errorsVec[i][1] < min)
            {
                min = errorsVec[i][1];
                idx = errorsVec[i][0];
            }
        }
        cout << "Minimal error: " << min << "\nOptimal n: " << idx << endl;
        ErrorGraphics(gnuplotiostreamFile, errorsVec);
    }
    else
    {
        do
        {
            (x->second)(start);
            errorsVec.push_back({ (double)start, this->errorb });
            cout << name << " ----- Error = " << this->errorb << endl;
            cout << name << " ----- Minimal m : " << start << "\n" << endl;
            start = start + steps;
        } while (this->errorb > Delta);
        if (graphics)
        {
            ErrorGraphics(gnuplotiostreamFile, errorsVec);
        }
    }
}

void Integration::ErrorGraphics(string gnuplotiostreamFile, vector<vector<double>> errorsVec, int start, int steps)
{
    // ����������� ������ 
    double y_min = errorsVec[0][1];
    double x_min = errorsVec[0][0];
    for (int i = 0; i < errorsVec.size(); i++)
    {
        if (errorsVec[i][1] < y_min)
        {
            y_min = errorsVec[i][1];
        }
        if (errorsVec[i][0] < x_min)
        {
            x_min = errorsVec[i][0];
        }
    }
    // ������������ ������
    double y_max = errorsVec[0][1];
    double x_max = errorsVec[0][0];
    for (int i = 0; i < errorsVec.size(); i++)
    {
        if (errorsVec[i][1] > y_max)
        {
            y_max = errorsVec[i][1];
        }
        if (errorsVec[i][0] > x_max)
        {
            x_max = errorsVec[i][0];
        }
    }
    Gnuplot gp(gnuplotiostreamFile); // ������ � Gnuplot
    gp << " set title 'Errors graphic'\n";
    gp << "set pointsize 1\n"
        << "set xrange[" << start - steps << ":" << x_max + steps << "]\n"
        << "set xtics " << steps << "\n"
        << "set yrange [" << -1.1 * y_min << ":" << 1.1 * y_max << "]\n"
        << "plot" << gp.file1d(errorsVec, "errorsVec.dat") << "with linespoints pointtype 7 title 'Errors', "
        << endl;
    system("PAUSE");
}

void Integration::CreateDataSet()
{
    this->x_arr.clear();
    this->result = 0;
    this->delta = (this->b - this->a) / (double)this->n; // ��� ������� ����� ������ ������������
    for (double i = this->a; i <= this->b; i += this->delta)
    {
        this->x_arr.push_back(i);
    }
}

double Integration::Legendre(int n, double x)
{
    if (n == 0)
        return 1.;
    if (n == 1)
        return x;
    n--;
    return (double)(((2. * n + 1) * x * Legendre(n, x)) / (n + 1.) - (n / (n + 1.)) * Legendre(n - 1, x));
}

double Integration::diffLegendre(int n, double x)
{
    if (n == 0) { return 0.; }
    if (n == 1) { return 1.; }
    double dLn = (n / (1 - x * x)) * (Legendre(n - 1, x) - x * Legendre(n, x));
    return dLn;
}

vector<double> Integration::LegendreRoot(int n)
{
    vector<double> roots(n);
    for (int i = 1; i <= n; i++)
    {
        double x_0 = cos(M_PI * (4. * i - 1) / (4. * n + 2));
        roots[n - i] = x_0;
    }
   
    for (int j = 0; j < n; j++)
    {
        double x_k = roots[j] - Legendre(n, roots[j]) / diffLegendre(n, roots[j]);
        roots[j] = x_k;
    }
    return roots;
}

// �������

double Integration::getP()
{
    return this->p;
}

double Integration::getResult()
{
    return this->result;
}

double Integration::getErrorb()
{
    return this->errorb;
}

double Integration::getMapleResult()
{
    return this->mapleResult;
}

// �������, ����������� ������ ���������� ��������������

void Integration::Rectangle(int n)
{   
    this->n = n;
    CreateDataSet();
    for (int i = 0; i < x_arr.size() - 1; i++)
    {
        this->result += y((x_arr[i + 1] + x_arr[i]) / 2.0);
    }
    this->result *= delta;
    ComputeError();
}

void Integration::Trapezoid(int n)
{
    this->n = n;
    CreateDataSet();
    for (int i = 1; i < x_arr.size() - 1; i++)
    {
        this->result += y(x_arr[i]);
    }
    this->result *= delta;
    this->result += delta * ((y(a) + y(b)) / 2.0);
    ComputeError();
}

void Integration::Simpson(int n)
{
    /*this->n = 2.0 * n;
    CreateDataSet();
    this->n = n;
    this->result += y(x_arr[0]);
    for (int i = 1; i < n; i++)
    {
        this->result += y(x_arr[2 * i - 1]);
    }
    this->result *= 4;
    for (int i = 1; i < n - 1; i++)
    {
        this->result += 2.0 * y(x_arr[2 * i]);
    }
    this->result += (y(x_arr.front()) + y(x_arr.back()));
    this->result *= delta;
    this->result /= 3.0;
    ComputeError();*/
    //if (n % 2 == 0)
    //{
    //    perror("Not odd number of nodes for Simpson integration!");
    //    exit(1);
    //}
    //this->n = n;
    //CreateDataSet();

    //// ����� �� ������ ��������
    //this->result += y(x_arr[0]);
    //for (int i = 2; i < this->n - 1; i += 2)
    //{
    //    this->result += 2 * y(x_arr[i]);
    //}
    //this->result += y(x_arr[this->n - 1]);

    //// ����� �� �������� ��������
    //for (int i = 1; i < this->n; i += 2)
    //{
    //    this->result += 4 * y(x_arr[i]);
    //}

    //this->result *= (this->b - this->a) / (this->n - 1) / 3;
    //ComputeError();

    this->n = 2.0 * n;
    CreateDataSet();
    for (int i = 1; i < this->n - 1; i += 2)
    {
        this->result += y(x_arr[i - 1]) + 4 * y(x_arr[i]) + y(x_arr[i + 1]);
    }
    this->result *= delta;
    this->result /= 3.0;
    ComputeError();
}

void Integration::Aitken(int n, double k, string name)
{
    func::iterator x = this->f.find(name);
    if (x == f.end()) // ���� ������ ����� ���
    {
        cout << "�an't find such a function, check the request!" << endl;
        system("PAUSE");
        return;
    }
    if ((k < 0.) || (k > 1.)) { cout << "Murat" << endl; return; }
    int h_1 = n;
    int h_2 = n / k;
    int h_3 = h_2 / k;
    //cout << h_1 << " " << h_2 << " " << h_3 << endl;
    auto K = [&](auto h)-> double { (x->second)(h); return result; };
    //cout << K(h_1) << " " << K(h_2) << " " << K(h_3) << endl;
    double B = (fabs(K(h_3) - K(h_2))) / fabs(K(h_2) - K(h_1));
    //cout << std::setprecision(20)<< B << endl;
    this->p = round((log(B) / log(k)));
   //cout << p << endl;
}

void Integration::Runge(int n1, int n2, string name, double k)
{
    func::iterator x = this->f.find(name);
    if (x == f.end()) // ���� ������ ����� ���
    {
        cout << "�an't find such a function, check the request!" << endl;
        system("PAUSE");
        return;
    }
    
    int maxH = max(n1, n2);
    Aitken(maxH, k, name);
    auto S = [&](auto h)-> double { (x->second)(h); return result; };
    double S1 = S(n1);
    double S2 = S(n2);
    double sigma = pow(k, this->p) / (pow(k, this->p) - 1.);
    this->result = sigma * S1 + (1. - sigma) * S2;
    ComputeError();
}

void Integration::Gauss(int n)
{
    vector<double> c(n);
    vector<double> x = this->LegendreRoot(n);

    for (int i = 0; i < n; i++)
    {
        c[i] = 2 / ((1 - pow(x[i], 2)) * pow(this->diffLegendre(n, x[i]), 2));
        //cout << c[i] << "  ";
    }
    //cout << endl;
    /*for (int i = 0; i < n; i++)
    {
        cout << x[i] << "  ";
    }*/
    //cout << endl;
    double res = 0.;
    for (int i = 0; i < n; i++)
    {
        res += c[i] * y(yGauss(x[i], this->a, this->b));    
    }
    this->result =((this->b-this->a)/2.) * res;
    ComputeError();
}

void Integration::Romberg(int n, double k, string name, double q)
{
    func::iterator x = this->f.find(name);
    if (x == f.end()) // ���� ������ ����� ���
    {
        cout << "�an't find such a function, check the request!" << endl;
        system("PAUSE");
        return;
    }
    vector<vector<double>> M1;
    vector<vector<double>> M2;
    vector<double> S_f;
    vector<double> H;
    vector<int> N;
    Aitken(n, k, name);
    do 
    {
        M1.clear();
        M2.clear();
        N.clear();
        S_f.clear();
        H.clear();
        for (int i = 0; i < q; i++)
        {
            N.push_back(n / pow(k, i));
            (x->second)(N[i]);
            S_f.push_back(this->result);
            H.push_back((this->b - this->a) * pow(k, i) / n);
        }
        M1.resize(q);
        for (auto& el : M1) { el.resize(q); }
        for (int i = 0; i < M1.size(); i++)
        {
            M1[i][0] = S_f[i];
            for (int j = 1; j < M1[i].size(); j++)
            {
                M1[i][j] = pow(H[i], this->p + j - 1);
            }
        }
        M2 = M1;
        for (int i = 0; i < M2.size(); i++)
        {
            M2[i][0] = 1;
        }
        this->result = Matrix::Determinant(M1) / Matrix::Determinant(M2);
        cout << Matrix::Determinant(M1) << "\t" << Matrix::Determinant(M2) << "\t" << this->result << endl;
        ComputeError();
        q++;
    } while (this->errorb > Delta);
}

void Integration::MonteCarlo(int n)
{
    auto gen = default_random_engine();
    auto dis = uniform_real_distribution<> (this->a, this->b);
    double S = 0.;
    for (int i = 0; i < n; i++)
    {
        S += y(dis(gen));
    }
    this->result = ((this->b - this->a)/(double)n) * S;
    ComputeError();
}



